-- SUMMARY --

QScience is an installation profile to set up and run "QScience instances" very easily.
QScience is a free, open source, distributed platform tailored to support the needs of 
modern scholarly communities. QScience offers a free, open source, web 2.0 venue for 
scientists to meet and discuss about science.

 -- VULNERABILITY IN VERSIONS <= 7.X-1.8 --

A vulnerability was discovered by @Pere Orga (Drupal Security team) in Patterns, one of the contributed modules which is used in this distribution (see https://www.drupal.org/node/2411539). The issue is solved in versions >= 7.X-1.8 of QScience. Versions of QScience <= 7.X-1.8 should update the patterns module to >= 7.x-2.2 or update the QScience distribution to >= 7.X-1.8.

-- INSTALLATION --

* Install as an usual Drupal site, see https://drupal.org/node/1089736 for further details.
